# Terraform Bootstrapper for Linux/Mac

Small project to manage versions of Terraform. You can specify a specific 
version or just set to latest to grab the latest version.

I may rewrite this using Python at some point. For now, it can run standalone
on most systems and fits most of my use cases, albeit a little noddy. 

**Disclaimer:**
This project is by no means 'production grade' code :) Use at your own risk. 

### MacOS Demo
![macos demo](docs/assets/terraform-bootstrap-mac-1.gif)

### Linux screenshot (Screencapture broken on my Arch workstation)
![linux demo](docs/assets/linux-example-1.png)

## Requirements
- jq
- curl

## Usage
This can be used standalone, but I created this to be used as a submodule within 
other infrastructure projects of mine. 

Run ``source ./venv.sh`` to create the virtual env within the project
directory or ``source ./venv.sh /PATH/TO/BIN`` to use another dest location.

You can modify the version or target architecture by editing the 
**conf/vars.sh** file. If nothing has been set, the script will do it's best
to identify the OS type and Arch of your machine.

### Use as a gitmodule
You can add this to your terraform projects but adding this repository as a 
gitmodule. To do this, first add the project and set the desired version.

```shell
export TERRAFORM_VERSION=x.x.x
git submodule add https://gitlab.com/zarrenspry/terraform-bootstrap.git && \
sed -r 's/(TERRAFORM_VERSION=)\".*\"/\1\"${TERRAFORM_VERSION}\"/' config/vars.sh
```
Next, add a .terraform-version file to the root of your project. For e.g.
```
echo "x.y.z" | tee .terraform-version
```

Now you can source the venv file to download Terraform and make it available for
the current session.

```shell
pushd terraform-bootstrap
source ./venv.sh
popd
```

## Todo

- Add checksum validation.
- Improvements to logging. i.e. configurable log levels and log output to file.
- Add some unit tests.
- Downgrade warning message?

---
author: @zarrenspry <zarrenspry@gmail.com>
---
