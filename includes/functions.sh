ERROR="\033[91m"
INFO="\033[92m"
NORMAL="\033[0m"
YELLOW="\033[93m"

sanity_checks() {
  if ! command -v jq &> /dev/null && [ -z ${TERRAFORM_VERSION} ]; then
    echo -e "${ERROR}ERROR:${NORMAL} \U274C jq was not found on the system and no version has been set. Please either install jq or set the TERRAFORM_VERSION variable to continue"; return 1
  fi
}

get_terraform() {
  # Determine Operating system
  case $(uname) in
    Linux)
      TARGET_OS="linux"
      echo -e "${INFO}INFO:${NORMAL} \U1F427 Linux system detected"
      ;;
    Darwin)
      TARGET_OS="darwin"
      echo -e "${INFO}INFO:${NORMAL} \U1F34E MacOS system detected"
      ;;
    *)
      echo -e "${ERROR}ERROR:${NORMAL} \U274C Target OS ${INFO}\`$(uname)\`${NORMAL} is not valid"; return 1
  esac

  # Determine Arch
  if [ -z ${TERRAFORM_ARCH} ]; then
    echo -e "${INFO}INFO:${NORMAL} \U23F1 Detecting operating system architecture..."
    case $(uname -m) in
      x86_64)
        TERRAFORM_ARCH="amd64"
        ;;
      arm64)
        TERRAFORM_ARCH="arm64"
        ;;
      arm|arm32)
        TERRAFORM_ARCH="arm"
        ;;
      x86)
        TERRAFORM_ARCH="386"
        ;;
      *)
        echo -e "${ERROR}ERROR:${NORMAL} \U274C This script was unable to determine your system architecture"; return 1
      esac
    echo -e "${INFO}INFO:${NORMAL} \U2714 ${TERRAFORM_ARCH} architecture detected"
  else
    case ${TERRAFORM_ARCH} in
      386|arm64|amd64|arm)
        echo -e "${INFO}INFO:${NORMAL} \U2714 The architecture was manually set to ${INFO}${TERRAFORM_ARCH}${NORMAL}"
        ;;
      *)
        echo -e "${ERROR}ERROR:${NORMAL} \U274C Target arch ${INFO}\`${TERRAFORM_ARCH}\`${NORMAL} is not valid. Please select from the following list\n - 386\n - arm64\n - amd64\n - arm"; return 1
        ;;
    esac
  fi

  # Attempt to collect version information from local file
  LOCAL_VARS_FILE=${$(find ${BASE_DIR}/.. -type f -name '.terraform-version' | head -1)##${BASE_DIR}/}
  if ! [ -z ${LOCAL_VARS_FILE} ]; then
    echo -e "${INFO}INFO:${NORMAL} \U203C a local version file was found within the root of this project"
    _TERRAFORM_VERSION=$(<${LOCAL_VARS_FILE})
    # Validate content and populate variable.
    if [[ ${_TERRAFORM_VERSION} =~ "^[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+$|^latest$" ]]; then
      TERRAFORM_VERSION="${_TERRAFORM_VERSION}"
    else
      echo -e "${ERROR}ERROR:${NORMAL} \U274C  The local version file ${LOCAL_VARS_FILE} failed the validation check. Please evaluate the file and try again"; return 1
    fi
  fi

  # Are we collecting the latest version?
  if [[ "${TERRAFORM_VERSION}" == "latest" ]] || [[ -z ${TERRAFORM_VERSION} ]]; then
    echo -e "${INFO}INFO:${NORMAL} \U23F1 Detecting latest version..."
    #TODO: Add some error handling here
    TERRAFORM_VERSION="$(curl -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq -j .current_version)"
    echo -e "${INFO}INFO:${NORMAL} \U2714 Latest version is ${INFO}${TERRAFORM_VERSION}${NORMAL}"
  fi

  # Set the target URL
  TERRAFORM_URL="https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_${TARGET_OS}_${TERRAFORM_ARCH}.zip"

  # Create the installation directory if it doesn't exist
  if [ ! -d "${INSTALLATION_DIR}/bin" ]; then
    echo -e "${INFO}INFO:${NORMAL} \U23F1 Creating ${INSTALLATION_DIR}/bin directory"
    mkdir -p "${INSTALLATION_DIR}/bin"
  fi

  # Check to see if a terraform binary already exists within the installation directory
  if ! type "${INSTALLATION_DIR}/bin/terraform" &> /dev/null; then
    # Unpack the zip file into the installation directory
    echo -e "${INFO}INFO:${NORMAL} \U23F1 Installing terraform to ${INSTALLATION_DIR}/bin/terraform"
    curl -s ${TERRAFORM_URL} | bsdtar -xf- -C ${INSTALLATION_DIR}/bin || { echo -e "${ERROR}ERROR:${NORMAL} \U274C Failed to download terraform"; return 1; }
    chmod +x ${INSTALLATION_DIR}/bin/terraform
    echo -e "${INFO}INFO:${NORMAL} \U1F389 Installation successful!"
  # Switch versions if binary already exists within the installation directory
  else
    terraform_installed_version="$(${INSTALLATION_DIR}/bin/terraform --version | grep -Eo '[[:digit:]]+.[[:digit:]]+.[[:digit:]]+$')"
    if [[ "${terraform_installed_version}" != ${TERRAFORM_VERSION} ]]; then
      if [ ${terraform_installed_version//./} -gt ${TERRAFORM_VERSION//./} ]; then
        echo -e "${YELLOW}WARNING:${NORMAL} \U203C The current deployed Terraform version ${YELLOW}${terraform_installed_version}${NORMAL} is ahead the target version ${INFO}${TERRAFORM_VERSION}${NORMAL}"
        SWAP_ACTION="downgrade"
      elif [ ${terraform_installed_version//./} -lt ${TERRAFORM_VERSION//./} ]; then
        echo -e "${YELLOW}WARNING:${NORMAL} \U203C The current deployed Terraform version ${YELLOW}${terraform_installed_version}${NORMAL} is behind of the target version ${INFO}${TERRAFORM_VERSION}${NORMAL}"
        SWAP_ACTION="upgrade"
      fi

      echo -e "Would you like me to ${SWAP_ACTION} to version ${TERRAFORM_VERSION}?\nType \`yes\` to upgrade or anything else to exit: \c"
      read -r _update
      if [[ "${_update}" == "yes" ]]; then
        echo -e "${INFO}INFO:${NORMAL} \U23F1 Cleaning up old binary ${INSTALLATION_DIR}/bin/terraform"
        rm -f ${INSTALLATION_DIR}/bin/terraform || echo -e "Failed to delete ${INSTALLATION_DIR}/bin/terraform"
        echo -e "${INFO}INFO:${NORMAL} \U23F1 Installing terraform to ${INSTALLATION_DIR}/bin/terraform"
        curl -s ${TERRAFORM_URL} | bsdtar -xf- -C ${INSTALLATION_DIR}/bin || { echo -e "${ERROR}ERROR:${NORMAL} \U274C Failed to download terraform"; return 1; }
        chmod +x ${INSTALLATION_DIR}/bin/terraform
        echo -e "${INFO}INFO:${NORMAL} \U1F389 Terraform version: ${INFO}${TERRAFORM_VERSION}${NORMAL} has been installed to path: ${INFO}${INSTALLATION_DIR}/bin/terraform${NORMAL}"
      else
        echo -e "${INFO}INFO:${NORMAL} \U2714 Ok, preserving installed version"
      fi
    else
      echo -e "${INFO}INFO:${NORMAL} \U2714 Terraform is already installed and up to date"
    fi
  fi
}
