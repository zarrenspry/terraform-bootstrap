#!/usr/bin/env bash

set -o pipefail

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
INSTALLATION_DIR=${1:-$BASE_DIR}

echo "$INSTALLATION_DIR"
source ${BASE_DIR}/config/vars.sh
source ${BASE_DIR}/includes/functions.sh

sanity_checks
get_terraform
export PATH=${INSTALLATION_DIR}/bin:${PATH}